#![cfg(test)]

use std::sync::{Arc, Mutex};

use crate::Locker;

#[test]
fn it_works() {
    let x = Arc::new(Mutex::new(false));
    let locker = Locker::new();
    let locker_inner = locker.clone();
    let x_inner = x.clone();
    std::thread::spawn(move || {
        let mut x = x_inner.lock().unwrap();
        *x = true;
        locker_inner.unlock();
    });
    locker.wait();
    let x = x.lock().unwrap();
    assert!(*x);
}
