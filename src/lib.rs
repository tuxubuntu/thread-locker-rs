mod tests;

use std::sync::{Arc, Condvar, Mutex};

#[derive(Debug, Clone)]
pub struct Locker(Arc<(Mutex<bool>, Condvar)>);

impl Locker {
    pub fn wait(self) {
        let pair = self.0;
        let &(ref lock, ref cvar) = &*pair;
        let mut started = lock.lock().unwrap();
        while !*started {
            started = cvar.wait(started).unwrap();
        }
    }
    pub fn unlock(self) {
        let pair = self.0;
        let &(ref lock, ref cvar) = &*pair;
        let mut started = lock.lock().unwrap();
        *started = true;
        cvar.notify_one();
    }
    pub fn new() -> Self {
        Locker(Arc::new((Mutex::new(false), Condvar::new())))
    }
}
